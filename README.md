<h3 style="text-align: center">
    BACKEND: INVENTORY SERVICE
</h3>

---

APIs Documentation
------------
- Tool: Swagger
- Link document: [http://localhost:8080/swagger-ui/index.html](http://localhost:8080/swagger-ui/index.html)

Code Conventions By Oracle
------------
- Link document: [https://www.oracle.com/java/technologies/javase/codeconventions-contents.html](https://www.oracle.com/java/technologies/javase/codeconventions-contents.html)

Installation
------------

Pre-conditions:
- JDK: [Oracle JDK version 8](https://www.oracle.com/java/technologies/javase/javase8-archive-downloads.html)
- IDE: IntelliJ IDEA Ultimate(recommended) or eclipse,...
- Database: [PostgreSQL](https://www.postgresql.org/download/) (recommended version 12.x)
- Libraries: [lombok](https://projectlombok.org/)
- Migration: [Flyway](https://flywaydb.org/)

### Clone repo

``` bash
git clone https://gitlab.com/vodinhthien/java-inventory-service.git
```

### Go to folder

``` bash
cd java-inventory-service/
```

### Pull images and run as containers for project(database, cache, message queue,...)
``` bash
docker-compose up -d
```

### Choose environment to run project

Copy this text into argument vms to run

```text
-Dspring.profiles.active=dev
```

<p style="text-align: center">
    <img src="/src/main/resources/static/images/image_add_args.PNG" alt="Illustration for adding arguments">
</p>

## What's included

Within the download you'll find the following directories and files, logically grouping common assets and providing both
compiled and minified variations. You'll see something like this:

```
<Root project>
├── src/                                                    # project root
│   ├── main/                                               # container java classes and resources
│   │   ├── java                                            # java folder - where storing packages and java classes
│   │   └── resources                                       # resources folder
│   │       ├── static                                      # static - where storing public files
│   │       ├── templates                                   # templates - where storing HTML template
│   │       └── application-<env_name>.properties           # env files
│   │
│   └── test/                                               # test folder - where storing testing files
│       └── ...
│
└── pom.xml

```