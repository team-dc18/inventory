package vn.com.tma.inventory.model.projection;

public interface OptionSummary {
    String getId();
    String getOptionContent();
}
