package vn.com.tma.inventory.model.response.error;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import vn.com.tma.inventory.model.response.AbstractResponse;

@Getter
@Setter
@AllArgsConstructor
public class AbstractErrorResponse extends AbstractResponse {
    protected int code;
    protected String message;
}
