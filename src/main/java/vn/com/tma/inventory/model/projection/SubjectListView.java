package vn.com.tma.inventory.model.projection;

public interface SubjectListView {
    String getId();
    String getCourseName();
}
