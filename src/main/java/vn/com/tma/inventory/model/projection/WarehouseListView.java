package vn.com.tma.inventory.model.projection;

public interface WarehouseListView {
    String getName();
    String getAddress();
    String getRepoNo();
}
