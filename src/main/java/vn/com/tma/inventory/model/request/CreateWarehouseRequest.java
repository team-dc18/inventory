package vn.com.tma.inventory.model.request;

import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

@Getter
@Setter
@AllArgsConstructor
@ApiModel(value = "Warehouse Creation Request")
public class CreateWarehouseRequest {
    @NotEmpty
    @Size(min = 3, max = 50)
    private String name;

    @NotEmpty
    @Size(min = 3, max = 50)
    private String repoNo;

    @NotEmpty
    @Size(min = 3, max = 50)
    private String longitude;

    @NotEmpty
    @Size(min = 3, max = 50)
    private String latitude;

    @NotEmpty
    @Size(min = 3, max = 255)
    private String address;
}
