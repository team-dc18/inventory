package vn.com.tma.inventory.model.projection;

public interface RoleInfoSummary {
    String getName();
}