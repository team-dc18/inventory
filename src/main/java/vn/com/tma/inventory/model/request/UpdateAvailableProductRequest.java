package vn.com.tma.inventory.model.request;

import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@ApiModel(value = "Update product available quantity request")
public class UpdateAvailableProductRequest {
    @NotNull
    private Long productId;

    private List<StockRequest> stocks;
}
