package vn.com.tma.inventory.model.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import java.io.Serializable;

@Entity(name = "Catalogs")
@Setter
@Getter
public class Catalog extends AbstractEntity implements Serializable {
    private long productId;
}
