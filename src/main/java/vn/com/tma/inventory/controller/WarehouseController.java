package vn.com.tma.inventory.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import vn.com.tma.inventory.model.entity.Warehouse;
import vn.com.tma.inventory.exception.movie.CustomMethodArgumentNotValidException;
import vn.com.tma.inventory.model.projection.WarehouseListView;
import vn.com.tma.inventory.model.request.CreateWarehouseRequest;
import vn.com.tma.inventory.model.request.UpdateAvailableProductRequest;
import vn.com.tma.inventory.model.request.UpdateWarehouseRequest;
import vn.com.tma.inventory.model.response.AbstractResponse;
import vn.com.tma.inventory.model.response.error.BaseErrorResponse;
import vn.com.tma.inventory.model.response.success.BaseResultResponse;
import vn.com.tma.inventory.model.response.success.PageInfo;
import vn.com.tma.inventory.model.response.success.PagingResultResponse;
import vn.com.tma.inventory.service.StockService;
import vn.com.tma.inventory.service.WarehouseService;
import vn.com.tma.inventory.util.ServerResponseUtil;

import javax.validation.Valid;

@RestController
@RequestMapping("/repositories")
@Api(tags = "APIs related to Warehouse")
@AllArgsConstructor
public class WarehouseController {
    private final WarehouseService warehouseService;
    private final StockService stockService;

    @ApiOperation(value = "Create a not existed warehouse", response = BaseResultResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = ServerResponseUtil.SUCCEED_CODE, message = ServerResponseUtil.STATUS_200_MESSAGE),
            @ApiResponse(code = ServerResponseUtil.BAD_REQUEST_CODE, message = ServerResponseUtil.STATUS_400_REASON),
            @ApiResponse(code = ServerResponseUtil.UNAUTHORIZED_CODE, message = ServerResponseUtil.STATUS_401_REASON),
            @ApiResponse(code = ServerResponseUtil.NOT_ALLOWED_CODE, message = ServerResponseUtil.STATUS_403_REASON),
            @ApiResponse(code = ServerResponseUtil.NOT_FOUND_DATA_CODE, message = ServerResponseUtil.STATUS_404_REASON),
            @ApiResponse(code = ServerResponseUtil.INTERNAL_SERVER_ERROR_CODE, message = ServerResponseUtil.STATUS_500_REASON)
    })
    @PostMapping
    public AbstractResponse createWarehouse(@Valid @RequestBody CreateWarehouseRequest createWarehouseRequest,
                                            BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            throw new CustomMethodArgumentNotValidException(bindingResult);
        } else {
            Warehouse createdWarehouse = warehouseService.createWarehouse(createWarehouseRequest);
            return new BaseResultResponse<>(HttpStatus.OK.value(), createdWarehouse);
        }
    }

    @ApiOperation(value = "Update a warehouse", response = BaseResultResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = ServerResponseUtil.SUCCEED_CODE, message = ServerResponseUtil.STATUS_200_MESSAGE),
            @ApiResponse(code = ServerResponseUtil.BAD_REQUEST_CODE, message = ServerResponseUtil.STATUS_400_REASON),
            @ApiResponse(code = ServerResponseUtil.UNAUTHORIZED_CODE, message = ServerResponseUtil.STATUS_401_REASON),
            @ApiResponse(code = ServerResponseUtil.NOT_ALLOWED_CODE, message = ServerResponseUtil.STATUS_403_REASON),
            @ApiResponse(code = ServerResponseUtil.NOT_FOUND_DATA_CODE, message = ServerResponseUtil.STATUS_404_REASON),
            @ApiResponse(code = ServerResponseUtil.INTERNAL_SERVER_ERROR_CODE, message = ServerResponseUtil.STATUS_500_REASON)
    })
    @PutMapping("{id}")
    public AbstractResponse updateWarehouse(@PathVariable Long id,
                                            @Valid @RequestBody UpdateWarehouseRequest updateWarehouseRequest,
                                            BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            throw new CustomMethodArgumentNotValidException(bindingResult);
        } else {
            Warehouse updatedWarehouse = warehouseService.updateWarehouse(id, updateWarehouseRequest);
            return new BaseResultResponse<>(HttpStatus.OK.value(), updatedWarehouse);
        }
    }

    @ApiOperation(value = "Get a warehouse by ID", response = BaseResultResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = ServerResponseUtil.SUCCEED_CODE, message = ServerResponseUtil.STATUS_200_MESSAGE),
            @ApiResponse(code = ServerResponseUtil.BAD_REQUEST_CODE, message = ServerResponseUtil.STATUS_400_REASON),
            @ApiResponse(code = ServerResponseUtil.UNAUTHORIZED_CODE, message = ServerResponseUtil.STATUS_401_REASON),
            @ApiResponse(code = ServerResponseUtil.NOT_ALLOWED_CODE, message = ServerResponseUtil.STATUS_403_REASON),
            @ApiResponse(code = ServerResponseUtil.NOT_FOUND_DATA_CODE, message = ServerResponseUtil.STATUS_404_REASON),
            @ApiResponse(code = ServerResponseUtil.INTERNAL_SERVER_ERROR_CODE, message = ServerResponseUtil.STATUS_500_REASON)
    })
    @GetMapping("{id}")
    public AbstractResponse getWarehouseById(@PathVariable Long id) {
        Warehouse result = warehouseService.findWarehouseById(id);
        return new BaseResultResponse<>(HttpStatus.OK.value(), result);
    }

    @ApiOperation(value = "List all existed warehouses", response = PagingResultResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = ServerResponseUtil.SUCCEED_CODE, message = ServerResponseUtil.STATUS_200_MESSAGE),
            @ApiResponse(code = ServerResponseUtil.BAD_REQUEST_CODE, message = ServerResponseUtil.STATUS_400_REASON),
            @ApiResponse(code = ServerResponseUtil.UNAUTHORIZED_CODE, message = ServerResponseUtil.STATUS_401_REASON),
            @ApiResponse(code = ServerResponseUtil.NOT_ALLOWED_CODE, message = ServerResponseUtil.STATUS_403_REASON),
            @ApiResponse(code = ServerResponseUtil.NOT_FOUND_DATA_CODE, message = ServerResponseUtil.STATUS_404_REASON),
            @ApiResponse(code = ServerResponseUtil.INTERNAL_SERVER_ERROR_CODE, message = ServerResponseUtil.STATUS_500_REASON)
    })
    @GetMapping
    public AbstractResponse listAllExistedWarehouses(@RequestParam(name = "page", required = false, defaultValue = "0") int page,
                                                     @RequestParam(name = "size", required = false, defaultValue = "25") int size,
                                                     @RequestParam(name = "query", required = false) String query,
                                                     @RequestParam(name = "sort", required = false, defaultValue = "name") String sort) {
        Page<WarehouseListView> resultPage;
        Pageable pageable = PageRequest.of(page, size, Sort.by(sort).ascending());
        if (query == null) {
            resultPage = warehouseService.listAllExistedWarehouses(pageable);
        } else {
            resultPage = warehouseService.listAllExistedWarehousesWithQuery(query, pageable);
        }

        return new PagingResultResponse<>(
                HttpStatus.OK.value(),
                resultPage.getContent(),
                new PageInfo(
                        page,
                        size,
                        (int) resultPage.getTotalElements(),
                        resultPage.getTotalPages()
                )
        );
    }

    @ApiOperation(value = "Delete a warehouse by ID", response = BaseResultResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = ServerResponseUtil.SUCCEED_CODE, message = ServerResponseUtil.STATUS_200_MESSAGE),
            @ApiResponse(code = ServerResponseUtil.BAD_REQUEST_CODE, message = ServerResponseUtil.STATUS_400_REASON),
            @ApiResponse(code = ServerResponseUtil.UNAUTHORIZED_CODE, message = ServerResponseUtil.STATUS_401_REASON),
            @ApiResponse(code = ServerResponseUtil.NOT_ALLOWED_CODE, message = ServerResponseUtil.STATUS_403_REASON),
            @ApiResponse(code = ServerResponseUtil.NOT_FOUND_DATA_CODE, message = ServerResponseUtil.STATUS_404_REASON),
            @ApiResponse(code = ServerResponseUtil.INTERNAL_SERVER_ERROR_CODE, message = ServerResponseUtil.STATUS_500_REASON)
    })
    @DeleteMapping("{id}")
    public AbstractResponse deleteWarehouseById(@PathVariable Long id) {
        int recordAfterDeleted = warehouseService.deletedWarehouseById(id);
        if (recordAfterDeleted == 1) {
            return new BaseResultResponse<>(
                    HttpStatus.OK.value(),
                    "Warehouse has ID = " + id + " was be deleted.");
        }

        return new BaseErrorResponse(
                HttpStatus.BAD_REQUEST.value(),
                "Warehouse has ID = " + id + " was not found for deleting. Nothing will be done!");
    }

    @ApiOperation(value = "Update product available quantity to repo", response = BaseResultResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = ServerResponseUtil.SUCCEED_CODE, message = ServerResponseUtil.STATUS_200_MESSAGE),
            @ApiResponse(code = ServerResponseUtil.BAD_REQUEST_CODE, message = ServerResponseUtil.STATUS_400_REASON),
            @ApiResponse(code = ServerResponseUtil.UNAUTHORIZED_CODE, message = ServerResponseUtil.STATUS_401_REASON),
            @ApiResponse(code = ServerResponseUtil.NOT_ALLOWED_CODE, message = ServerResponseUtil.STATUS_403_REASON),
            @ApiResponse(code = ServerResponseUtil.NOT_FOUND_DATA_CODE, message = ServerResponseUtil.STATUS_404_REASON),
            @ApiResponse(code = ServerResponseUtil.INTERNAL_SERVER_ERROR_CODE, message = ServerResponseUtil.STATUS_500_REASON)
    })
    @PutMapping("product-stock")
    public AbstractResponse updateAvailableProduct(@Valid @RequestBody UpdateAvailableProductRequest updateAvailableProductRequest,
                                                   BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            throw new CustomMethodArgumentNotValidException(bindingResult);
        } else {
            stockService.updateAvailable(updateAvailableProductRequest);
            return new BaseErrorResponse(HttpStatus.OK.value(), "Update stock was successfully");
        }
    }
}
