package vn.com.tma.inventory.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import vn.com.tma.inventory.model.entity.Warehouse;
import vn.com.tma.inventory.exception.CustomException;
import vn.com.tma.inventory.service.WarehouseService;
import vn.com.tma.inventory.validator.WarehouseValidator;

import javax.validation.Valid;

@RestController
@RequestMapping("warehouse")
public class WarehouseV2Controller {
    @Autowired
    private WarehouseService warehouseService;

    @PostMapping("/")
    @ResponseStatus(HttpStatus.CREATED)
    public Warehouse createWareHouse(
            @Valid @RequestBody WarehouseValidator warehouseData
//            Authentication authentication  //authen if any
    ){
        return this.warehouseService.create(warehouseData);
    }

    @GetMapping("/")
    public Page<Warehouse> search(
            @RequestParam(value = "page", defaultValue = "1", required = false) int page,
            @RequestParam(value = "size", defaultValue = "10", required = false) int pageSize,
            @RequestParam(value = "searchValue", defaultValue = "", required = false) String searchValue
    ) throws CustomException {
        return this.warehouseService.search(page, pageSize, searchValue);
    }

    @GetMapping("/{id}/")
    public Warehouse getOne( @PathVariable int id) {
        return this.warehouseService.findOne(id);
    }

    @PutMapping("/{id}/")
    public Warehouse updateStaff(
            @PathVariable int id,
            @Valid @RequestBody WarehouseValidator warehouseData
    ){
        Warehouse instance = this.warehouseService.findOne(id);
        return this.warehouseService.update(instance, warehouseData);
    }

}
