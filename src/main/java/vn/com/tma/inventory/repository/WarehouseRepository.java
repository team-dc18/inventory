package vn.com.tma.inventory.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;
import vn.com.tma.inventory.model.entity.Warehouse;
import vn.com.tma.inventory.model.projection.WarehouseListView;
import vn.com.tma.inventory.util.choice.DeleteStatus;

import java.util.Optional;

public interface WarehouseRepository extends JpaRepository<Warehouse, Long> {

    Optional<Warehouse> findWarehouseByNameAndIsDeleted(String name, DeleteStatus isDeleted);

    Optional<Warehouse> findWarehouseByIdAndIsDeleted(long id, DeleteStatus isDeleted);

    @Query("select wh from Warehouses wh where wh.isDeleted = :deleted")
    Page<WarehouseListView> findByIsDeleted(@Param("deleted") DeleteStatus isDeleted, Pageable pageable);

    @Query("select wh from Warehouses wh where wh.name like %:name% and wh.isDeleted = :deleted " +
            "OR wh.repoNo like %:repoNo% and wh.isDeleted = :deleted " +
            "OR wh.address like %:address% and wh.isDeleted = :deleted ")
    Page<WarehouseListView> findByNameLikeOrRepoNoLikeOrAddressLike(@Param("name") String name,
                                                                    @Param("repoNo") String repoNo,
                                                                    @Param("address") String address,
                                                                    @Param("deleted") DeleteStatus isDeleted,
                                                                    Pageable pageable);

    @Transactional
    @Modifying
    @Query("update Warehouses u set u.isDeleted = ?1 where u.id = ?2")
    int setIsDeletedFor(DeleteStatus isDeleted, long id);

    //    V2Controller
    @Query("select case when count(w) > 0 then true else false end from Warehouses w where w.name = ?1 or w.repoNo = ?1")
    boolean exists(String name);

    @Query("select w from Warehouses w where " +
            "lower(w.name) like concat('%', lower(?1), '%')")
    Page<Warehouse> search(String searchValue, Pageable pageable);

    Optional<Warehouse> findById(long id);

}
