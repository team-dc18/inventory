package vn.com.tma.inventory.util.choice;

import com.fasterxml.jackson.annotation.JsonFormat;

@JsonFormat(shape = JsonFormat.Shape.NUMBER)
public enum Role {
    ROLE_SUPER_ADMIN,
    ROLE_ADMIN,
    ROLE_EMPLOYEE,
}