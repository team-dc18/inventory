package vn.com.tma.inventory.util.choice;

import com.fasterxml.jackson.annotation.JsonFormat;

@JsonFormat(shape = JsonFormat.Shape.NUMBER)
public enum ActiveStatus {
    IN_ACTIVE,
    ACTIVATION
}
