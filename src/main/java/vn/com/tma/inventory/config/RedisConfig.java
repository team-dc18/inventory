package vn.com.tma.inventory.config;

import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.concurrent.ConcurrentMapCache;
import org.springframework.cache.support.SimpleCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Arrays;

@Configuration
@EnableCaching
public class RedisConfig {
    @Bean
    public CacheManager cacheManager() {
        SimpleCacheManager cacheManager = new SimpleCacheManager();
        Cache singlePostSeoUrlCache = new ConcurrentMapCache("singleWarehouse");
        Cache menuListCache = new ConcurrentMapCache("warehouseList");

        cacheManager.setCaches(Arrays.asList(singlePostSeoUrlCache, menuListCache));
        return cacheManager;
    }
}
