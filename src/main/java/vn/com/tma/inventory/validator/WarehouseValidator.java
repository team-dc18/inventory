package vn.com.tma.inventory.validator;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import vn.com.tma.inventory.util.choice.ActiveStatus;

import javax.validation.constraints.*;

@Data
@Slf4j
public class WarehouseValidator {
    @NotBlank(message = "Field name is required")
    @Size(max = 50, message = "Field name cannot be over 50 characters")
    private String name;

    @NotBlank(message = "Field repoNo is required")
    @Size(max = 50, message = "Field repoNo cannot be over 50 characters")
    private String repoNo;

    @NotNull(message = "Field longitude is required")
    private String longitude;

    @NotNull(message = "Field latitude is required")
    private String latitude;

    @NotNull(message = "Field latitude is required")
    private String address;

    private ActiveStatus status;
}
