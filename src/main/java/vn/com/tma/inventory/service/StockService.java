package vn.com.tma.inventory.service;

import org.springframework.transaction.annotation.Transactional;
import vn.com.tma.inventory.model.request.UpdateAvailableProductRequest;

public interface StockService {

    /**
     * Update a warehouse
     *
     * @param updateAvailableProductRequest: some info for updating stock for a product
     */
    @Transactional
    void updateAvailable(UpdateAvailableProductRequest updateAvailableProductRequest);
}
