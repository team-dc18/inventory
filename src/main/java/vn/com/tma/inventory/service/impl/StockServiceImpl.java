package vn.com.tma.inventory.service.impl;

import lombok.AllArgsConstructor;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;
import vn.com.tma.inventory.model.entity.Catalog;
import vn.com.tma.inventory.model.entity.CompositeKey;
import vn.com.tma.inventory.model.entity.Stock;
import vn.com.tma.inventory.exception.movie.NotFoundException;
import vn.com.tma.inventory.model.request.StockRequest;
import vn.com.tma.inventory.model.request.UpdateAvailableProductRequest;
import vn.com.tma.inventory.repository.CatalogRepository;
import vn.com.tma.inventory.repository.StockRepository;
import vn.com.tma.inventory.service.StockService;

import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class StockServiceImpl implements StockService {
    private final CatalogRepository catalogRepository;
    private final StockRepository stockRepository;
    private final MessageSource messages;

    @Override
    public void updateAvailable(UpdateAvailableProductRequest updateAvailableProductRequest) {
        Optional<Catalog> catalogOptional = catalogRepository.findByProductId(updateAvailableProductRequest.getProductId());
        if (catalogOptional.isPresent()) {
            Catalog localCatalog = catalogOptional.get();
            List<StockRequest> stockRequests = updateAvailableProductRequest.getStocks();
            for (StockRequest st : stockRequests) {
                Optional<Stock> localStockOptional = stockRepository.findDistinctByCompositeKey_CatalogIdAndCompositeKey_WarehouseIdAndTypeId(localCatalog.getId(), st.getRepoId(), st.getTypeId());
                Stock existedStock;

                if (localStockOptional.isPresent()) {
                    existedStock = localStockOptional.get();
                    existedStock.setAvailable(st.getAvailable() >= 0 ? st.getAvailable() : existedStock.getAvailable());
                    existedStock.setPrice(st.getPrice() >= 0 ? st.getPrice() : existedStock.getPrice());
                } else {
                    existedStock = new Stock();
                    CompositeKey compositeKey = new CompositeKey();
                    compositeKey.setCatalogId(localCatalog.getId());
                    compositeKey.setWarehouseId(st.getRepoId());

                    existedStock.setCompositeKey(compositeKey);
                    existedStock.setTypeId(st.getTypeId());
                    existedStock.setAvailable(st.getAvailable());
                    existedStock.setPrice(st.getPrice());
                    existedStock.setSku(st.getSku());

                }
                stockRepository.save(existedStock);

            }
        } else {
            throw new NotFoundException(String.format(messages.getMessage("product.get.error.not-found", null, null), updateAvailableProductRequest.getProductId()));
        }
    }
}
