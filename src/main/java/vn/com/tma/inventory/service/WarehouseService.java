package vn.com.tma.inventory.service;

import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;
import vn.com.tma.inventory.model.entity.Warehouse;
import vn.com.tma.inventory.exception.CustomException;
import vn.com.tma.inventory.model.projection.WarehouseListView;
import vn.com.tma.inventory.model.request.CreateWarehouseRequest;
import vn.com.tma.inventory.model.request.UpdateWarehouseRequest;
import vn.com.tma.inventory.validator.WarehouseValidator;

public interface WarehouseService {

    /**
     * Create a warehouse if not existed
     *
     * @param createWarehouseRequest: some info for creating warehouse
     * @return Warehouse
     */
    @Transactional
    Warehouse createWarehouse(CreateWarehouseRequest createWarehouseRequest);

    /**
     * Update a warehouse
     *
     * @param updateWarehouseRequest: some info for updating warehouse
     * @return Warehouse
     */
    @Transactional
    @CachePut(value = "singleWarehouse", key = "#id")
    Warehouse updateWarehouse(long id, UpdateWarehouseRequest updateWarehouseRequest);

    /**
     * Find a warehouse with isDeleted = 0 (not deleted yet) by ID
     * @return WarehouseListView
     */
    @Transactional(readOnly = true)
    @Cacheable(value = "singleWarehouse", key = "#id")
    Warehouse findWarehouseById(long id);

    /**
     * List all existed warehouses with isDeleted = 0 (not deleted yet) and without query
     * @param pageable: some info to pagination
     * @return WarehouseListView
     */
    @Transactional(readOnly = true)
    Page<WarehouseListView> listAllExistedWarehouses(Pageable pageable);

    /**
     * List all existed warehouses with isDeleted = 0 (not deleted yet) and with query
     * @param query:    input to search warehouse (name, repoNo, address)
     * @param pageable: some info to pagination
     * @return WarehouseListView
     */
    @Transactional(readOnly = true)
    Page<WarehouseListView> listAllExistedWarehousesWithQuery(String query, Pageable pageable);

    /**
     * Delete a warehouse by ID
     * => Set isDeleted = 1
     * @return int
     */
    @Transactional
    @CacheEvict(value = "singleWarehouse", key = "#id")
    int deletedWarehouseById(long id);

    Warehouse create(WarehouseValidator validatedData);

    Page<Warehouse> search(int page, int pageSize, String searchValue) throws CustomException;

    Warehouse findOne(int id);

    Warehouse update(Warehouse warehouse,WarehouseValidator validatedData);

}
