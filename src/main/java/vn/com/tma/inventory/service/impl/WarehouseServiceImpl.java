package vn.com.tma.inventory.service.impl;

import lombok.AllArgsConstructor;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import vn.com.tma.inventory.model.entity.Warehouse;
import vn.com.tma.inventory.exception.CustomException;
import vn.com.tma.inventory.util.choice.DeleteStatus;
import vn.com.tma.inventory.exception.ServiceException;
import vn.com.tma.inventory.exception.movie.NotFoundException;
import vn.com.tma.inventory.model.projection.WarehouseListView;
import vn.com.tma.inventory.model.request.CreateWarehouseRequest;
import vn.com.tma.inventory.model.request.UpdateWarehouseRequest;
import vn.com.tma.inventory.repository.WarehouseRepository;
import vn.com.tma.inventory.service.WarehouseService;
import vn.com.tma.inventory.validator.WarehouseValidator;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityNotFoundException;
import java.util.Optional;

@Service
@AllArgsConstructor
public class WarehouseServiceImpl implements WarehouseService {
    private final WarehouseRepository warehouseRepository;
    private final MessageSource messages;

    @Override
    public Warehouse createWarehouse(CreateWarehouseRequest createWarehouseRequest) {
        // Preprocessing input data
        createWarehouseRequest.setName(createWarehouseRequest.getName().trim());
        createWarehouseRequest.setRepoNo(createWarehouseRequest.getRepoNo().trim());
        createWarehouseRequest.setLongitude(createWarehouseRequest.getLongitude().trim());
        createWarehouseRequest.setLatitude(createWarehouseRequest.getLatitude().trim());
        createWarehouseRequest.setAddress(createWarehouseRequest.getAddress().trim());

        Optional<Warehouse> localWarehouseOptional = warehouseRepository.findWarehouseByNameAndIsDeleted(createWarehouseRequest.getName(), DeleteStatus.NOT_DELETED);

        if (localWarehouseOptional.isPresent()) {
            throw new ServiceException(String.format(messages.getMessage("warehouse.create.error.name-existed", null, null), createWarehouseRequest.getName()));
        }

        Warehouse createdWarehouse = new Warehouse();
        createdWarehouse.setName(createWarehouseRequest.getName());
        createdWarehouse.setRepoNo(createWarehouseRequest.getRepoNo());
        createdWarehouse.setLongitude(createWarehouseRequest.getLongitude());
        createdWarehouse.setLatitude(createWarehouseRequest.getLatitude());
        createdWarehouse.setAddress(createWarehouseRequest.getAddress());

        return warehouseRepository.save(createdWarehouse);
    }

    @Override
    public Warehouse updateWarehouse(long id, UpdateWarehouseRequest updateWarehouseRequest) {
        Optional<Warehouse> localWarehouseOptional = warehouseRepository.findWarehouseByIdAndIsDeleted(id, DeleteStatus.NOT_DELETED);
        Warehouse localWarehouse = localWarehouseOptional.get();
        if (localWarehouseOptional.isPresent()) {

            localWarehouse.setName(updateWarehouseRequest.getName() != null ? updateWarehouseRequest.getName().trim() : localWarehouse.getName());
            localWarehouse.setRepoNo(updateWarehouseRequest.getRepoNo() != null ? updateWarehouseRequest.getRepoNo().trim() : localWarehouse.getRepoNo());
            localWarehouse.setLongitude(updateWarehouseRequest.getLongitude() != null ? updateWarehouseRequest.getLongitude().trim() : localWarehouse.getLongitude());
            localWarehouse.setLatitude(updateWarehouseRequest.getLatitude() != null ? updateWarehouseRequest.getLatitude().trim() : localWarehouse.getLatitude());
            localWarehouse.setAddress(updateWarehouseRequest.getAddress() != null ? updateWarehouseRequest.getAddress().trim() : localWarehouse.getAddress());

            localWarehouse = warehouseRepository.save(localWarehouse);
        } else {
            throw new NotFoundException(String.format(messages.getMessage("warehouse.get.error.not-found", null, null), updateWarehouseRequest.getName()));
        }
        return localWarehouse;
    }

    @Override
    public Warehouse findWarehouseById(long id) {
        Optional<Warehouse> localWarehouseOptional = warehouseRepository.findWarehouseByIdAndIsDeleted(id, DeleteStatus.NOT_DELETED);
        if(localWarehouseOptional.isPresent()) {
            return localWarehouseOptional.get();
        } else {
            throw new NotFoundException(String.format(messages.getMessage("warehouse.get.error.not-found", null, null), id));
        }
    }

    @Override
    public Page<WarehouseListView> listAllExistedWarehouses(Pageable pageable) {
        return warehouseRepository.findByIsDeleted(DeleteStatus.NOT_DELETED, pageable);
    }

    @Override
    public Page<WarehouseListView> listAllExistedWarehousesWithQuery(String query, Pageable pageable) {
        query = query.trim();
        return warehouseRepository.findByNameLikeOrRepoNoLikeOrAddressLike(query, query, query, DeleteStatus.NOT_DELETED, pageable);
    }

    @Override
    public int deletedWarehouseById(long id) {
        Optional<Warehouse> localWarehouseOptional = warehouseRepository.findWarehouseByIdAndIsDeleted(id, DeleteStatus.NOT_DELETED);
        if(localWarehouseOptional.isPresent()) {
            return warehouseRepository.setIsDeletedFor(DeleteStatus.DELETED, id);
        }
        return -1;
    }

    @Override
    public Warehouse create(WarehouseValidator validatedData) {
        Warehouse warehouse = new Warehouse();

        if (this.warehouseRepository.exists(validatedData.getName())) {
            throw new EntityExistsException("Name is already in use");
        }

        if (this.warehouseRepository.exists(validatedData.getRepoNo())) {
            throw new EntityExistsException("Ware house code is already existed");
        }
        createOrUpdateWareHouse(validatedData, warehouse);
        return this.warehouseRepository.save(warehouse);
    }

    @Override
    public Page<Warehouse> search(int page, int pageSize, String searchValue) throws CustomException {
        page--;
        if (page < 0 || pageSize < 0){
            throw new CustomException("Invalid page");
        }
        Pageable pageable = PageRequest.of(page, pageSize, Sort.by("name").ascending());
        return this.warehouseRepository.search(searchValue, pageable);
    }

    @Override
    public Warehouse findOne(int id) {
            Optional<Warehouse> warehouse = this.warehouseRepository.findById(id);
            if (warehouse.isPresent()) {
                return warehouse.get();
            }
            throw new EntityNotFoundException("No ware house could be found");
    }

    @Override
    public Warehouse update(Warehouse instance, WarehouseValidator validatedData) {

        if (!instance.getName().equals(validatedData.getName()) &&
                this.warehouseRepository.exists(validatedData.getName())) {
            throw new EntityExistsException("Name is already in use");
        }

        if (!instance.getRepoNo().equals(validatedData.getRepoNo()) &&
                this.warehouseRepository.exists(validatedData.getRepoNo())) {
            throw new EntityExistsException("Code is already in use");
        }
        createOrUpdateWareHouse(validatedData, instance);
        return this.warehouseRepository.save(instance);
    }


    private void createOrUpdateWareHouse(WarehouseValidator validatedData, Warehouse instance){
        instance.setName(validatedData.getName());
        instance.setRepoNo(validatedData.getRepoNo());
        instance.setLatitude(validatedData.getLatitude());
        instance.setLongitude(validatedData.getLongitude());
        instance.setAddress(validatedData.getAddress());
        instance.setStatus(validatedData.getStatus());
    }
}
