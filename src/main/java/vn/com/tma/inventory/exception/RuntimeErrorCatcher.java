package vn.com.tma.inventory.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
//import org.springframework.security.access.AccessDeniedException;
//import org.springframework.security.core.AuthenticationException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import javax.persistence.EntityExistsException;
import javax.persistence.EntityNotFoundException;
import java.time.format.DateTimeParseException;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;

@RestControllerAdvice
public class RuntimeErrorCatcher {

    @ExceptionHandler(EntityNotFoundException.class)
    protected ResponseEntity<Object> handleEntityNotFoundException(Exception ex) {
        Map<String, Object> bodyRes = new LinkedHashMap<>();
        bodyRes.put("timestamp", new Date());
        bodyRes.put("status", HttpStatus.NOT_FOUND.value());
        bodyRes.put("error", HttpStatus.NOT_FOUND.name());
        bodyRes.put("message", ex.getMessage());
        return new ResponseEntity<>(bodyRes, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler({
            EntityExistsException.class,
            CustomException.class,
            DateTimeParseException.class,
            NumberFormatException.class,
    })
    protected ResponseEntity<Object> handleBadRequestException(Exception ex) {
        Map<String, Object> bodyRes = new LinkedHashMap<>();
        bodyRes.put("timestamp", new Date());
        bodyRes.put("status", HttpStatus.BAD_REQUEST.value());
        bodyRes.put("error", HttpStatus.BAD_REQUEST.name());
        String message = ex.getMessage();
        bodyRes.put("message", message);
        return new ResponseEntity<>(bodyRes, HttpStatus.BAD_REQUEST);
    }

//    @ExceptionHandler({AuthenticationException.class, AccessDeniedException.class})
//    protected ResponseEntity<Object> handleAuthenticationException(Exception ex) {
//        Map<String, Object> bodyRes = new LinkedHashMap<>();
//        bodyRes.put("timestamp", new Date());
//        bodyRes.put("status", HttpStatus.FORBIDDEN.value());
//        bodyRes.put("error", HttpStatus.FORBIDDEN.name());
//        bodyRes.put("message", ex.getMessage());
//        return new ResponseEntity<>(bodyRes, HttpStatus.FORBIDDEN);
//    }
}
