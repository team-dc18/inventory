package vn.com.tma.inventory.exception;

public class CustomException extends Exception {
    public CustomException(String message) {
        super(message);
    }
}
