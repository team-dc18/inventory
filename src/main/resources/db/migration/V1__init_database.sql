create table catalogs
(
    id           bigserial
        constraint catalogs_pkey
            primary key,
    created_date timestamp default CURRENT_TIMESTAMP not null,
    updated_date timestamp default CURRENT_TIMESTAMP,
    product_id   bigint                              not null
);

alter table catalogs
    owner to postgres;

create table stocks
(
    catalog_id   bigint             not null,
    warehouse_id bigint             not null,
    available    bigint             not null,
    price        bigint             not null,
    sku          varchar(255),
    status       smallint default 1 not null,
    type_id      bigint             not null,
    constraint stocks_pkey
        primary key (catalog_id, warehouse_id)
);

alter table stocks
    owner to postgres;

create table warehouses
(
    id           bigserial
        constraint warehouses_pkey
            primary key,
    created_date timestamp default CURRENT_TIMESTAMP not null,
    updated_date timestamp default CURRENT_TIMESTAMP,
    address      text,
    is_deleted   smallint  default 0                 not null,
    latitude     varchar(255),
    longitude    varchar(255),
    name         varchar(255),
    repo_no      varchar(255),
    status       smallint  default 1                 not null
);

alter table warehouses
    owner to postgres;

